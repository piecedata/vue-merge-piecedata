export function isPlainObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
}

/**
 * Returns a deeply cloned object without reference.
 * Copied from Vuex.
 * @type {Object}
 */
export function deepClone(obj) {
    if (Array.isArray(obj)) {
        return obj.map(deepClone);
    }
    if (isPlainObject(obj)) {
        const cloned = {};
        const keys = Object.keys(obj);
        for (let i = 0, l = keys.length; i < l; i += 1) {
            const key = keys[i];
            cloned[key] = deepClone(obj[key]);
        }
        return cloned;
    }

    return obj;
}

// export function deepMerge(...args) {
//     /* eslint-disable prefer-spread */
//     return merge.apply(null, args);
// }

// deep merge/clone defaults

// this function is the same as deepClone
// except it will remove return an empty array
export function deepCloneDefaults(val) {
    if (isPlainObject(val)) {
        const res = {};
        /* eslint-disable guard-for-in, no-restricted-syntax */
        for (const key in val) {
            res[key] = deepCloneDefaults(val[key]);
        }
        return res;
    }
    if (Array.isArray(val)) {
        return [];
    }

    return val;
}

export function deepMergeDefaults(target, source) {
    const targetType = Object.prototype.toString.call(target);
    const sourceType = Object.prototype.toString.call(source);

    if (process.env.NODE_ENV === 'development'
        && sourceType !== '[object Undefined]'
        && sourceType !== '[object Null]'
        && targetType !== '[object Null]'
        && targetType !== sourceType) {
        console.warn(`deepMergeDefaults(): ${targetType} !== ${sourceType}`);
        console.log(target);
        console.log(source);
    }

    if (targetType === '[object Object]') {
        if (sourceType === '[object Object]') {
            const destination = {};

            Object.keys(target)
                .forEach((key) => {
                    destination[key] = deepMergeDefaults(target[key], source[key]);
                });

            return destination;
        }

        return deepCloneDefaults(target);
    }

    if (targetType === '[object Array]') {
        if (sourceType === '[object Array]') {
            return source.map(source => target[0]
                ? deepMergeDefaults(target[0], source)
                : deepClone(source));
        }

        return [];
    }

    // source type cast
    if (sourceType !== '[object Undefined]') {
        if (targetType === sourceType
            || targetType === '[object Null]') {
            return source;
        }

        if (targetType === '[object Boolean]') {
            return Boolean(source);
        }

        if (targetType === '[object String]'
            && sourceType === '[object Number]') {
            return String(source);
        }

        if (targetType === '[object Number]') {
            const nr = Number(source);

            if (!Number.isNaN(nr)) {
                return nr;
            }
        }
    }

    return target;
}
